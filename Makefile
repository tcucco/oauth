build:
	make clean
	python setup.py build sdist

clean:
	rm -rf build/ dist/ .coverage .mypy_cache *.egg-info
	find . -iname __pycache__ -delete
	find . -iname *.pyc -delete
	find . -iname .DS_Store -delete

coverage:
	coverage run test.py
	coverage report

install:
	pip install --upgrade pip setuptools
	pip install -r requirements.txt

test:
	python test.py

run-provider:
	FLASK_APP=service_provider_server.py FLASK_DEBUG=1 flask run --port=5000

run-consumer:
	FLASK_APP=consumer_server.py FLASK_DEBUG=1 flask run --port=5001
