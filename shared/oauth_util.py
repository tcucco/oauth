'''Contains utility methods.'''
import base64
import hmac
import secrets
import time
import urllib.parse
from functools import (partial)
from typing import (
    Any,
    Callable,
    Iterable,
    Mapping,
    MutableMapping,
    Optional,
    Set,
    Tuple,
    cast,
)

ENCODING = 'UTF-8'
INVALID_HEADER_VALUE_FORMAT_STRING = (
    "Invalid OAuth header value: {0}."
    """Must be formatted as 'key="value"'"""
)
VALID_OAUTH_SIGNATURE_METHODS = {'HMAC-SHA1'}
VALID_OAUTH_VERSIONS = {'1.0'}
REQUIRED_OAUTH_PARAMS = {
    'oauth_consumer_key',
    'oauth_nonce',
    'oauth_signature',
    'oauth_signature_method',
    'oauth_timestamp',
}
REQUIRED_OAUTH_REQUEST_PARAMS = REQUIRED_OAUTH_PARAMS | {
    'oauth_callback',
}
REQUIRED_OAUTH_ACCESS_PARAMS = REQUIRED_OAUTH_PARAMS | {
    'oauth_token',
    'oauth_verifier',
}
REQUIRED_OAUTH_USE_PARAMS = REQUIRED_OAUTH_PARAMS | {
    'oauth_token',
}
TIMESTAMP_DRIFT = 60

RequestParams = Mapping[str, str]
OAuthParamValidator = Callable[[RequestParams], Optional[str]]
ParamPairs = Iterable[Tuple[str, str]]

def try_parse_int(val: str) -> Tuple[bool, Optional[int]]:
  '''Tries to parse an int.'''
  try:
    return (True, int(val))
  except (ValueError, TypeError):
    return (False, None)

def with_defaults(data: Mapping, defaults: Mapping) -> Mapping:
  '''Applies default values to a mapping.'''
  return {**defaults, **data}

def create_token(length: int = 32) -> str:
  '''Creates a random token.'''
  token = secrets.token_bytes(length)
  return token.hex()

def encode_param(value: str) -> str:
  '''Encodes a value for serialization.'''
  if not value:
    return ''
  return urllib.parse.quote(value, safe='')

def unencode_param(value: str) -> str:
  '''Unencodes a serialized value.'''
  if not value:
    return ''
  return urllib.parse.unquote(value)

def encode_param_pair(key: str, value: str, quote_value: bool = False) -> str:
  '''Encodes a key-value pair.'''
  key_enc = encode_param(key)
  val_enc = encode_param(value)

  if quote_value:
    val_enc = '"{}"'.format(val_enc)

  return '{0}={1}'.format(key_enc, val_enc)

def encode_param_pairs(params: ParamPairs) -> str:
  '''Encodes many key value pairs.'''
  return '&'.join(encode_param_pair(key, val) for (key, val) in params)

def encode_oauth_header_pairs(params: ParamPairs) -> str:
  '''Encodes parameters to be used in an oauth header.'''
  return ','.join(encode_param_pair(key, val, True) for (key, val) in params)

def mapping_to_param_pairs(val: Mapping[str, Any]) -> ParamPairs:
  '''Truns a mapping into param pairs.'''
  return [(str(key), '' if val is None else str(val)) for (key, val) in dict(val).items()]

def parse_oauth_header_values(param_str: str) -> RequestParams:
  '''Parses OAuth value string into a mapping of keys to values.'''
  if not param_str:
    return {}

  param_pairs = param_str.split(',')
  params: MutableMapping[str, str] = {}

  for pair in param_pairs:
    spair = pair.strip()
    pair_parts = spair.split('=')

    if len(pair_parts) != 2:
      raise ValueError(INVALID_HEADER_VALUE_FORMAT_STRING.format(spair))

    (key, value) = pair_parts

    if len(value) < 2 or (value[0], value[-1]) != ('"', '"'):
      raise ValueError(INVALID_HEADER_VALUE_FORMAT_STRING.format(spair))

    params[key] = unencode_param(value[1:-1])

  return params

def parse_oauth_header(header_value: str) -> Tuple[str, RequestParams]:
  '''Parses an oauth header value into a (scheme, params) tuple.'''
  header_parts = header_value.strip().split(' ', 1)

  if len(header_parts) == 2:
    (scheme, param_str) = header_parts
  else:
    (scheme, param_str) = (header_parts[0], '')

  return (scheme, parse_oauth_header_values(param_str))

def validate_oauth_params(
    required_params: Set[str],
    valid_signature_methods: Set[str],
    valid_versions: Set[str],
    params: RequestParams,
) -> Optional[str]:
  '''Validates oauth parameters.'''
  missing_params = sorted(required_params - set(params.keys()))

  if missing_params:
    return 'Missing OAuth parameters: {0}'.format(', '.join(missing_params))

  if params['oauth_signature_method'] not in valid_signature_methods:
    return 'Invalid OAuth signature method. Must be one of: {0}'.format(
        ', '.join(valid_signature_methods),
    )

  if params.get('oauth_version', '1.0') not in valid_versions:
    return 'Invalid OAuth version. Must be one of: {0}'.format(', '.join(valid_versions))

  (is_valid_timestamp, timestamp) = try_parse_int(params['oauth_timestamp'])
  timestamp = cast(int, timestamp)

  if not is_valid_timestamp or timestamp < 0:
    return 'Invalid OAuth timestamp. Must be a positive integer.'

  if abs(time.time() - timestamp) > TIMESTAMP_DRIFT:
    return 'Invalid OAuth timestamp. Must be within {} seconds of server time.'.format(
        TIMESTAMP_DRIFT,
    )

  return None

validate_oauth_request = partial(
    validate_oauth_params,
    REQUIRED_OAUTH_REQUEST_PARAMS,
    VALID_OAUTH_SIGNATURE_METHODS,
    VALID_OAUTH_VERSIONS,
)
validate_oauth_access = partial(
    validate_oauth_params,
    REQUIRED_OAUTH_ACCESS_PARAMS,
    VALID_OAUTH_SIGNATURE_METHODS,
    VALID_OAUTH_VERSIONS,
)
validate_oauth_use = partial(
    validate_oauth_params,
    REQUIRED_OAUTH_USE_PARAMS,
    VALID_OAUTH_SIGNATURE_METHODS,
    VALID_OAUTH_VERSIONS,
)

def build_signature_base_string(
    http_method: str,
    request_url: str,
    request_parameters: ParamPairs,
):
  '''Builds an OAuth signature base string.'''
  encoded_method = encode_param(http_method.upper())
  encoded_url = encode_param(request_url.lower())
  params = '&'.join(
      '{0}={1}'.format(encode_param(k), encode_param(v))
      for (k, v)
      in sorted(request_parameters)
  )
  encoded_params = encode_param(params)
  return '&'.join([encoded_method, encoded_url, encoded_params])

def calculate_oauth_signature(
    consumer_secret: str,
    token_secret: Optional[str],
    base_string: str,
) -> str:
  '''Calculates an OAuth signature from a pair of secrets and a signature base string.'''
  key = consumer_secret + '&' + (token_secret or '')
  digest = hmac.new(
      key.encode(ENCODING),
      base_string.encode(ENCODING),
      digestmod='SHA1',
  ).digest()
  signature = base64.b64encode(digest)
  return signature.decode(ENCODING)

def add_query_args_to_url(url: str, params: ParamPairs) -> str:
  '''Adds query args to a URL.'''
  # NOTE: This does not properly handle URLs that have a fragment
  joiner = '?' if url.find('?') == -1 else '&'
  return '{0}{1}{2}'.format(url, joiner, encode_param_pairs(params))

def build_oauth_redirect(request_token: Mapping[str, Any]) -> str:
  '''Returns a callback url built from a request token.'''
  url = request_token['callback_url']
  if url == 'oob':
    raise Exception('Invalid callback url for redirect')
  return add_query_args_to_url(
      url,
      [
          ('oauth_token', request_token['token']),
          ('oauth_verifier', request_token['verifier']),
      ],
  )

def build_oauth_header(
    consumer_secret: str,
    token_secret: Optional[str],
    method: str,
    url: str,
    auth_map: Mapping[str, str],
    request_items: ParamPairs = None,
) -> str:
  '''Builds an OAuth Authorization header value.'''
  auth_map = with_defaults(
      auth_map,
      {
          'oauth_nonce': create_token(),
          'oauth_signature_method': 'HMAC-SHA1',
          'oauth_version': '1.0',
          'oauth_timestamp': str(round(time.time())),
      },
  )
  auth_item_list = list(auth_map.items())
  if request_items:
    all_items = auth_item_list + list(request_items)
  else:
    all_items = auth_item_list

  base_string = build_signature_base_string(method, url, all_items)
  signature = calculate_oauth_signature(consumer_secret, token_secret, base_string)
  auth_pairs = sorted(auth_item_list + [('oauth_signature', signature)])
  header_value = encode_oauth_header_pairs(auth_pairs)

  return 'OAuth ' + header_value
