# pylint: disable=missing-docstring
from unittest import TestCase
from functools import partial
from shared import oauth_util as util

class OAuthUtilTest(TestCase):
  def test_encode_param(self):
    raw = 'ab/cd'
    expected = 'ab%2Fcd'
    actual = util.encode_param(raw)
    self.assertEqual(expected, actual)

  def test_test_try_parse_int(self):
    with self.subTest('None'):
      self.assertEqual((False, None), util.try_parse_int(None))

    with self.subTest('empty string'):
      self.assertEqual((False, None), util.try_parse_int(''))

    with self.subTest('letters'):
      self.assertEqual((False, None), util.try_parse_int('a'))

    with self.subTest('floating point number string'):
      self.assertEqual((False, None), util.try_parse_int('42.5'))

    with self.subTest('integer number string'):
      self.assertEqual((True, 42), util.try_parse_int('42'))

    with self.subTest('integer'):
      self.assertEqual((True, 42), util.try_parse_int(42))

  def test_encode_param_pair(self):
    self.assertEqual('name=trey%20cucco', util.encode_param_pair('name', 'trey cucco'))

  def test_encode_param_pairs(self):
    pairs = [
        ('name', 'trey cucco'),
        ('favorite color', 'red'),
    ]
    self.assertEqual('name=trey%20cucco&favorite%20color=red', util.encode_param_pairs(pairs))

  def test_mapping_to_param_pairs(self):
    self.assertEqual(
        [('a', 'b'), ('b', '1'), ('c', '')],
        util.mapping_to_param_pairs({
            'a': 'b',
            'b': 1,
            'c': None,
        }),
    )

  def test_parse_oauth_header_values(self):
    self.assertEqual({}, util.parse_oauth_header_values(''))

    self.assertEqual(
        {
            'oauth_nonce': '123',
            'oauth_timestamp': '123987456',
            'oauth_method': 'HMAC-SHA1',
        },
        util.parse_oauth_header_values(
            'oauth_nonce="123",oauth_timestamp="123987456", oauth_method="HMAC-SHA1"',
        ),
    )

    with self.assertRaises(ValueError):
      util.parse_oauth_header_values('oauth_nonce=123')

    with self.assertRaises(ValueError):
      util.parse_oauth_header_values('oauth_nonce="123')

    with self.assertRaises(ValueError):
      util.parse_oauth_header_values('oauth_nonce:"123"')

    with self.assertRaises(ValueError):
      util.parse_oauth_header_values("oauth_nonce='123'")

    with self.assertRaises(ValueError):
      util.parse_oauth_header_values('oauth_nonce="123",oauth_timestamp=')

    with self.assertRaises(ValueError):
      util.parse_oauth_header_values('oauth_nonce="123",oauth_timestamp')

  def test_parse_oauth_header(self):
    self.assertEqual(
        ('OAuth', {'oauth_nonce': '123', 'oauth_timestamp': '123987456'}),
        util.parse_oauth_header('OAuth oauth_nonce="123", oauth_timestamp="123987456"'),
    )

  def test_validate_oauth_params(self):
    validate = partial(
        util.validate_oauth_params,
        util.REQUIRED_OAUTH_REQUEST_PARAMS,
        util.VALID_OAUTH_SIGNATURE_METHODS,
        util.VALID_OAUTH_VERSIONS,
    )
    with self.subTest('missing all keys'):
      result1 = validate({})
      self.assertEqual(
          result1,
          'Missing OAuth parameters: oauth_callback, oauth_consumer_key, oauth_nonce, '
          'oauth_signature, oauth_signature_method, oauth_timestamp',
      )

    with self.subTest('missing one key'):
      result2 = validate({
          'oauth_callback': 'http://example.com',
          'oauth_consumer_key': '123123',
          'oauth_signature': '123asdf123',
          'oauth_signature_method': 'HMAC-SHA1',
          'oauth_timestamp': '123123123',
      })
      self.assertEqual(
          result2,
          'Missing OAuth parameters: oauth_nonce',
      )

    valid_params = {
        'oauth_callback': 'http://example.com',
        'oauth_consumer_key': '123123',
        'oauth_nonce': '54321',
        'oauth_signature': '123asdf123',
        'oauth_signature_method': 'HMAC-SHA1',
        'oauth_timestamp': '123123123',
    }

    with self.subTest('invalid signature method'):
      result3 = validate({**valid_params, 'oauth_signature_method': 'PLAINTEXT'})
      self.assertEqual(
          result3,
          'Invalid OAuth signature method. Must be one of: HMAC-SHA1',
      )

    with self.subTest('invalid oauth_version'):
      result4 = validate({**valid_params, 'oauth_version': '1.0a'})
      self.assertEqual(
          result4,
          'Invalid OAuth version. Must be one of: 1.0',
      )

    with self.subTest('iso oauth_timestamp'):
      result5 = validate({**valid_params, 'oauth_timestamp': '2019-05-07T13:30:00Z'})
      self.assertEqual(
          result5,
          'Invalid OAuth timestamp. Must be a positive integer.',
      )

    with self.subTest('negative oauth_timestamp'):
      result6 = validate({**valid_params, 'oauth_timestamp': '-123456789'})
      self.assertEqual(
          result6,
          'Invalid OAuth timestamp. Must be a positive integer.',
      )

  def test_build_signature_base_string(self):
    result = util.build_signature_base_string(
        'get',
        'Http://www.Example.com',
        [
            ('b', '2'),
            ('a', '1'),
            ('b', '1'),
        ],
    )
    self.assertEqual(
        'GET&http%3A%2F%2Fwww.example.com&a%3D1%26b%3D1%26b%3D2',
        result,
    )

  def test_calculate_oauth_signature(self):
    # From the oauth 1.0a docs: https://oauth.net/core/1.0a/#anchor46
    params = {
        'oauth_consumer_key': 'dpf43f3p2l4k3l03',
        'oauth_token': 'nnch734d00sl2jdk',
        'oauth_signature_method': 'HMAC-SHA1',
        'oauth_timestamp': '1191242096',
        'oauth_nonce': 'kllo9940pd9333jh',
        'oauth_version': '1.0',
        'file': 'vacation.jpg',
        'size': 'original',
    }
    base_string = util.build_signature_base_string(
        'GET',
        'http://photos.example.net/photos',
        params.items(),
    )
    self.assertEqual(
        'GET&http%3A%2F%2Fphotos.example.net%2Fphotos&file%3Dvacation.jpg'
        '%26oauth_consumer_key%3Ddpf43f3p2l4k3l03%26oauth_nonce%3Dkllo9940pd9333jh'
        '%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1191242096'
        '%26oauth_token%3Dnnch734d00sl2jdk%26oauth_version%3D1.0%26size%3Doriginal',
        base_string,
    )
    consumer_secret = 'kd94hf93k423kf44'
    token_secret = 'pfkkdhi9sl3r4s00'
    signature = util.calculate_oauth_signature(consumer_secret, token_secret, base_string)
    self.assertEqual('tR3+Ty81lMeYAr/Fid0kMTYa/WM=', signature)
