'''Contains a SQLite database wrapper.'''
from contextlib import contextmanager
import sqlite3 as sql
from typing import (
    Any,
    List,
    Mapping,
    Optional,
    cast,
)

Record = Mapping[str, Any]
Records = List[Record]

class DB:
  '''SQLite DB Wrapper.'''
  def __init__(self, path: str) -> None:
    self.path = path
    self.conn: Optional[sql.Connection] = None

  @property
  def is_open(self) -> bool:
    '''Returns if a connection is present or not.'''
    return self.conn is not None

  def connect(self) -> 'DB':
    '''Creates a connection if not already open.'''
    if not self.is_open:
      self.conn = sql.connect(self.path)
      self.conn.row_factory = sql.Row
    return self

  def close(self) -> 'DB':
    '''Closes the connection if open.'''
    if self.is_open:
      cast(sql.Connection, self.conn).close()
      self.conn = None
    return self

  @contextmanager
  def transaction(self):
    '''Executes a code block in a transaction.'''
    try:
      yield self
      self.conn.commit()
    except:
      self.conn.rollback()
      raise

  def get_cursor(self) -> sql.Cursor:
    '''Returns a cursor for the current connection.'''
    return cast(sql.Connection, self.conn).cursor()

  def execute(self, query: str, params: Optional[List[Any]] = None) -> sql.Cursor:
    '''Execurte a query'''
    cursor = self.get_cursor()
    if params:
      cursor.execute(query, params)
    else:
      cursor.execute(query)
    return cursor

  def fetch_one(self, query: str, params: Optional[List[Any]] = None) -> Optional[Record]:
    '''Returns the first result (or None) from a query.'''
    return self.execute(query, params).fetchone()

  def fetch_all(self, query: str, params: Optional[List[Any]] = None) -> Records:
    '''Returns all results from a query.'''
    return self.execute(query, params).fetchall()
