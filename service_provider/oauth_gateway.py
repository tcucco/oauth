'''Contains methods for interacting with OAuth data.'''
from typing import (
    Any,
    Mapping,
    Optional,
)
from shared.db import (DB, Record)

class OAuthGateway:
  '''Contains methods for accessing oauth tables.'''
  def __init__(self, db: DB) -> None:
    self.db = db

  def get_consumer_by_id(self, consumer_id: int) -> Optional[Record]:
    '''Finds a consumer by id.'''
    return self.db.fetch_one(
        'select * from consumers where id = ?',
        [consumer_id],
    )

  def get_consumer_by_key(self, consumer_key: str) -> Optional[Record]:
    '''Finds a consumer by their key.'''
    return self.db.fetch_one(
        'select * from consumers where key = ?',
        [consumer_key],
    )

  def create_request_token(self, params: Mapping[str, Any]) -> int:
    '''Creates a new unauthorized request token.'''
    cursor = self.db.execute(
        'insert into request_tokens (consumer_id, token, secret, callback_url, verifier, user_id, created_at) '
        'values (?, ?, ?, ?, ?, null, current_timestamp)',
        [
            params['consumer_id'],
            params['token'],
            params['secret'],
            params['callback_url'],
            params['verifier'],
        ],
    )
    token_id = cursor.lastrowid
    return token_id

  def get_request_token_by_id(self, token_id: int) -> Optional[Record]:
    '''Finds a request token by id.'''
    return self.db.fetch_one('select * from request_tokens where id = ?', [token_id])

  def get_request_token_by_token(self, token: str) -> Optional[Record]:
    '''Finds a request token by token.'''
    return self.db.fetch_one('select * from request_tokens where token = ?', [token])

  def authorize_request_token(self, user_id: int, token_id: int) -> None:
    '''Authorizes a request token for a user.'''
    self.db.execute(
        'update request_tokens '
        'set user_id = ? '
        'where id = ? and user_id is null',
        [
            user_id,
            token_id,
        ]
    )

  def delete_request_token(self, token_id: int) -> None:
    '''Deletes a request token.'''
    self.db.execute('delete from request_tokens where id = ?', [token_id])

  def create_access_token(self, params: Mapping[str, Any]) -> int:
    '''Creates a new access token.'''
    cursor = self.db.execute(
        'insert into access_tokens (consumer_id, user_id, token, secret, created_at) '
        'values (?, ?, ?, ?, current_timestamp)',
        [
            params['consumer_id'],
            params['user_id'],
            params['token'],
            params['secret'],
        ],
    )
    token_id = cursor.lastrowid
    return token_id

  def get_access_token_by_id(self, token_id: int) -> Optional[Record]:
    '''Finds an access token by id.'''
    return self.db.fetch_one('select * from access_tokens where id = ?', [token_id])

  def get_access_token_by_token(self, token: str) -> Optional[Record]:
    '''Finds an access token by token.'''
    return self.db.fetch_one('select * from access_tokens where token = ?', [token])

  def create_nonce(self, params: Mapping[str, Any]) -> int:
    '''Creates a nonce record.'''
    cursor = self.db.execute(
        'insert into nonces (consumer_id, timestamp, nonce, created_at) '
        'values (?, ?, ?, current_timestamp)',
        [
            params['consumer_id'],
            params['timestamp'],
            params['nonce'],
        ],
    )
    nonce_id = cursor.lastrowid
    return nonce_id

  def get_nonce(
      self,
      consumer_id: int,
      timestamp: int,
      nonce: str,
  ) -> Optional[Record]:
    '''Finds the nonce record for a given consumer and timestamp.'''
    return self.db.fetch_one(
        'select * from nonces where consumer_id = ? and timestamp = ? and nonce = ?',
        [consumer_id, timestamp, nonce],
    )

  def get_user_by_id(self, user_id: int) -> Optional[Record]:
    '''Finds a user by id.'''
    return self.db.fetch_one('select * from users where id = ?', [user_id])

  def get_user_by_username(self, username: str) -> Optional[Record]:
    '''Finds a user by username.'''
    return self.db.fetch_one('select * from users where username = ?', [username])
