'''Utility functions to bridge Flask to OAuth.'''
from typing import (List, Tuple)
from flask import (Request, Response)
from shared import oauth_util as util

def normalize_request_params(request: Request) -> List[Tuple[str, str]]:
  '''Returns a Flask Request params (query, body, auth header) in a normalized form.'''
  request_items = list(request.args.items(True))

  (auth_scheme, auth_params) = util.parse_oauth_header(request.headers.get('authorization', ''))

  if auth_scheme == 'OAuth':
    request_items += list(auth_params.items())

  if request.content_type == 'application/x-www-form-urlencoded':
    body_items = list(request.form.items(True))
    request_items += body_items
  return sorted(request_items)

def build_response(kvps: util.ParamPairs, status_code: int = 200) -> Response:
  '''Builds a x-www-form-urlencoded response.'''
  body = util.encode_param_pairs(kvps)
  return Response(body, status_code, mimetype='application/x-www-form-urlencoded')

def build_error_response(error: str, status_code: int = 400) -> Response:
  '''Builds an error response.'''
  return build_response([('error', error)], status_code)
