'''Utility methods for setting up and working with the oauth database.'''
from contextlib import contextmanager
from shared.db import (DB)

DB_PATH = 'oauthdb.sqlite3'

def initialize_database(db: DB):
  '''Sets up the database structure.'''
  db.execute('''
    create table if not exists users (
      id integer not null primary key,
      username text not null unique,
      created_at text not null
    )
  ''')

  db.execute('''
    create table if not exists consumers (
      id integer not null primary key,
      name text not null,
      key text not null unique,
      secret text not null,
      created_at text not null
    )
  ''')

  db.execute('''
    create table if not exists request_tokens (
      id integer not null primary key,
      consumer_id integer not null references consumers (id),
      token text not null unique,
      secret text not null,
      callback_url text not null,
      verifier text not null,
      user_id integer default null references users (id),
      created_at text not null
    )
  ''')

  db.execute('''
    create table if not exists access_tokens (
      id integer not null primary key,
      consumer_id integer not null references consumers (id),
      user_id integer not null references user (id),
      token text not null unique,
      secret text not null,
      created_at text not null
    )
  ''')

  db.execute('''
    create table if not exists nonces (
      id integer not null primary key,
      consumer_id integer not null references consumers (id),
      timestamp integer not null,
      nonce text not null,
      created_at text not null,

      unique (consumer_id, timestamp, nonce)
    )
  ''')

@contextmanager
def use_database():
  '''Context manager for using database.'''
  db = DB(DB_PATH)
  db.connect()
  initialize_database(db)
  try:
    yield db
  finally:
    db.close()
