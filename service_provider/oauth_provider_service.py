'''Contains an OAuth Provider Service class.'''
from typing import (Optional, Tuple, cast)
from shared import oauth_util as util
from shared.db import (Record)
from .oauth_gateway import (OAuthGateway)

Consumer = Record
OAuthToken = Record

class OAuthProviderService:
  '''Providers a high-level interface to the OAuthGateway.'''
  def __init__(self, gateway: OAuthGateway) -> None:
    self.gateway = gateway

  def get_consumer_by_id(self, consumer_id: int) -> Optional[Record]:
    '''Finds a consumer by id.'''
    return self.gateway.get_consumer_by_id(consumer_id)

  def get_consumer_by_key(self, consumer_key: str) -> Optional[Record]:
    '''Finds a consumer by key.'''
    return self.gateway.get_consumer_by_key(consumer_key)

  def create_request_token(self, consumer_id: int, callback_url: str) -> Record:
    '''Create and return a new unauthorized request token.'''
    params = {
        'consumer_id': consumer_id,
        'token': util.create_token(),
        'secret': util.create_token(),
        'callback_url': callback_url,
        'verifier': util.create_token(),
    }
    with self.gateway.db.transaction():
      token_id = self.gateway.create_request_token(params)
    return cast(Record, self.gateway.get_request_token_by_id(token_id))

  def get_request_token_by_token(self, token: str) -> Optional[Record]:
    '''Finds a request token by token.'''
    return self.gateway.get_request_token_by_token(token)

  def authorize_request_token(self, user_id: int, token_id: int) -> None:
    '''Authorizes a request token for a user.'''
    with self.gateway.db.transaction():
      self.gateway.authorize_request_token(user_id, token_id)

  def create_access_token(self, consumer: Record, request_token: Record, verifier: str) -> Record:
    '''Creates an access token from a request token.'''
    if not request_token:
      raise Exception('Request token not found.')

    if request_token['consumer_id'] != consumer['id']:
      raise Exception('Request token was not issued to you.')

    if not request_token['user_id']:
      raise Exception('Request token has not yet been authorized.')

    if request_token['verifier'] != verifier:
      raise Exception('Invalid request token verifier.')

    with self.gateway.db.transaction():
      params = {
          'consumer_id': request_token['consumer_id'],
          'user_id': request_token['user_id'],
          'token': util.create_token(),
          'secret': util.create_token(),
      }
      access_token_id = self.gateway.create_access_token(params)
      self.gateway.delete_request_token(request_token['id'])

    access_token = self.gateway.get_access_token_by_id(access_token_id)
    return cast(Record, access_token)

  def use_nonce(self, consumer_id: int, timestamp: int, nonce: str) -> bool:
    '''If a nonce has already been used, return False. Otherwise return True.'''
    existing = self.gateway.get_nonce(consumer_id, timestamp, nonce)

    if existing:
      return False

    with self.gateway.db.transaction():
      self.gateway.create_nonce(
          {
              'consumer_id': consumer_id,
              'timestamp': timestamp,
              'nonce': nonce,
          },
      )

    return True

  def get_user_by_id(self, user_id: int) -> Optional[Record]:
    '''Finds a user by id.'''
    return self.gateway.get_user_by_id(user_id)

  def get_user_by_username(self, username: str) -> Optional[Record]:
    '''Finds a user by username.'''
    return self.gateway.get_user_by_username(username)

  def validate_request(
      self,
      param_validator: util.OAuthParamValidator,
      method: str,
      base_url: str,
      request_items: util.RequestParams,
      action: str = 'use' # TODO: Not happy with this part of the implementation
  ) -> Tuple[Consumer, Optional[OAuthToken]]:
    '''Validates a request.'''
    request_params = dict(request_items)
    oauth_validation = param_validator(request_params)

    if oauth_validation:
      raise Exception(oauth_validation)

    consumer = self.get_consumer_by_key(request_params['oauth_consumer_key'])
    if not consumer:
      raise Exception('Invalid consumer key')

    token_key = request_params.get('oauth_token')
    if token_key:
      if action == 'use':
        oauth_token = self.gateway.get_access_token_by_token(token_key)
      elif action == 'access':
        oauth_token = self.get_request_token_by_token(token_key)
      if not oauth_token:
        raise Exception('Invalid OAuth token')
      token_secret = oauth_token['secret']
    else:
      oauth_token = None
      token_secret = ''

    if not self.use_nonce(
        consumer['id'],
        int(request_params['oauth_timestamp']),
        request_params['oauth_nonce'],
    ):
      raise Exception('Nonce has already been used.')

    signature_items = [(k, v) for (k, v) in request_items if k not in {'oauth_signature', 'realm'}]
    base_string = util.build_signature_base_string(method, base_url, signature_items)
    signature = util.calculate_oauth_signature(
        consumer['secret'],
        token_secret,
        base_string,
    )
    if request_params['oauth_signature'] != signature:
      raise Exception('Invalid OAuth Signature')

    return (consumer, oauth_token)
