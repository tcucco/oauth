'''Contains methods for accessing consumer data.'''
import json
import os

class ConsumerData:
  '''Contains methods for accessing consumer data.'''
  def __init__(self, filename: str) -> None:
    self.filename = filename

  def load_consumer_data(self) -> dict:
    '''Loads consumer data.'''
    if os.path.exists(self.filename):
      with open(self.filename, 'r') as handle:
        return json.load(handle)
    return {}

  def persist_consumer_data(self, data: dict) -> dict:
    '''Saves consumer data.'''
    with open(self.filename, 'w') as handle:
      json.dump(data, handle, indent=2)
    return data

  def merge_consumer_data(self, add_data: dict) -> dict:
    '''Merges data into the consumer data, persists it, and returns the updated data.'''
    data = self.load_consumer_data()
    data.update(add_data)
    return self.persist_consumer_data(data)
