'''Consumer API'''
import urllib.parse
from flask import (Flask, redirect, render_template, request)
import requests
import shared.oauth_util as u
from consumer.data import ConsumerData

app = Flask(__name__) # pylint: disable=invalid-name

CONSUMER_KEY = '42'
CONSUMER_SECRET = 'gandalfthegrey'
PROVIDER_BASE = 'http://localhost:5000'
CREATE_REQUEST_TOKEN_URL = PROVIDER_BASE + '/oauth/request-tokens'
AUTHORIZE_REQUEST_TOKEN_URL = PROVIDER_BASE + '/oauth/request-tokens/authorize'
CREATE_ACCESS_TOKEN_URL = PROVIDER_BASE + '/oauth/access-tokens'
GET_USER_DATA_URL = PROVIDER_BASE + '/user'
VALUES_FILENAME = '/tmp/consumer_data.json'

@app.route('/')
def hello():
  '''Health check endpoint.'''
  return 'Hello, world!'

@app.route('/integrations', methods=['GET'])
def render_request_integration_form():
  '''Renders an integration form.'''
  return render_template('request-integration.html')

@app.route('/integrations', methods=['POST'])
def initiate_integration():
  '''Begins the OAuth integration process.'''
  auth_map = {
      'oauth_consumer_key': CONSUMER_KEY,
      'oauth_callback': 'http://localhost:5001/integrations/authorized',
  }
  url = CREATE_REQUEST_TOKEN_URL
  auth_header = u.build_oauth_header(CONSUMER_SECRET, None, 'POST', url, auth_map)
  response = requests.post(url, headers={'Authorization': auth_header})
  response_values = urllib.parse.parse_qs(response.text)

  if response.status_code == 200:
    token = response_values['oauth_token'][0]
    secret = response_values['oauth_secret'][0]
    data = ConsumerData(VALUES_FILENAME)
    data.merge_consumer_data({'request_token': token, 'request_secret': secret})

    return redirect(u.add_query_args_to_url(
        AUTHORIZE_REQUEST_TOKEN_URL,
        [('oauth_token', token)],
    ))

  message = response_values['error'][0]
  return render_template('request-integration.html', message=message)

@app.route('/integrations/authorized', methods=['GET'])
def request_access_token():
  '''Requests an access token after a successful request authorization.'''
  request_token = request.args['oauth_token']
  request_verifier = request.args['oauth_verifier']
  data = ConsumerData(VALUES_FILENAME)
  oauth_data = data.merge_consumer_data({'request_verifier': request_verifier})

  auth_map = {
      'oauth_consumer_key': CONSUMER_KEY,
      'oauth_token': request_token,
      'oauth_verifier': request_verifier,
  }
  url = CREATE_ACCESS_TOKEN_URL
  auth_header = u.build_oauth_header(
      CONSUMER_SECRET,
      oauth_data['request_secret'],
      'POST',
      url,
      auth_map,
  )

  response = requests.post(url, headers={'Authorization': auth_header})
  response_values = urllib.parse.parse_qs(response.text)

  if response.status_code == 200:
    access_token = response_values['oauth_token'][0]
    access_secret = response_values['oauth_secret'][0]
    data = ConsumerData(VALUES_FILENAME)
    oauth_data = data.merge_consumer_data({
        'access_token': access_token,
        'access_secret': access_secret,
    })
    return redirect('http://localhost:5001/me')

  message = response_values['error'][0]
  return render_template('request-integration.html', message=message)

@app.route('/me', methods=['GET'])
def show_me():
  '''Pulls your uiser data from the integrated service and displays it.'''
  data = ConsumerData(VALUES_FILENAME)
  oauth_data = data.load_consumer_data()
  auth_map = {
      'oauth_consumer_key': CONSUMER_KEY,
      'oauth_token': oauth_data['access_token'],
  }
  url = GET_USER_DATA_URL
  auth_header = u.build_oauth_header(
      CONSUMER_SECRET,
      oauth_data['access_secret'],
      'GET',
      url,
      auth_map,
  )

  response = requests.get(url, headers={'Authorization': auth_header})
  response_values = urllib.parse.parse_qs(response.text)

  return render_template(
      'show-integration-user.html',
      **{k: v[0] for (k, v) in response_values.items()},
  )
