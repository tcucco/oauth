'''Service Provider API'''
from flask import (Flask, redirect, render_template, request)
from shared import oauth_util as util
from service_provider.data import (use_database)
from service_provider.flask_util import (
    build_error_response,
    build_response,
    normalize_request_params,
)
from service_provider.oauth_gateway import (OAuthGateway)
from service_provider.oauth_provider_service import (OAuthProviderService)

app = Flask(__name__) # pylint: disable=invalid-name

@app.route('/')
def hello():
  '''Health check endpoint.'''
  return 'Hello, world!'

@app.route('/user')
def get_user():
  '''Gets the user associated to the oauth token.'''
  with use_database() as db:
    service = OAuthProviderService(OAuthGateway(db))
    request_items = normalize_request_params(request)

    try:
      (_, token) = service.validate_request(
          util.validate_oauth_use,
          request.method,
          request.base_url,
          request_items,
          'use',
      )
    except Exception as ex:
      return build_error_response(str(ex))

    user_id = token['user_id']
    user = service.get_user_by_id(user_id)
    return build_response(util.mapping_to_param_pairs(user))

@app.route('/oauth/request-tokens', methods=['POST'])
def create_request_token():
  '''Create an unauthorized request token.'''
  with use_database() as db:
    service = OAuthProviderService(OAuthGateway(db))
    request_items = normalize_request_params(request)

    try:
      (consumer, _) = service.validate_request(
          util.validate_oauth_request,
          request.method,
          request.base_url,
          request_items,
          'request',
      )
    except Exception as ex:
      return build_error_response(str(ex))

    request_params = dict(request_items)
    request_token = service.create_request_token(
        consumer['id'],
        request_params['oauth_callback'],
    )

  body = [
      ('oauth_token', request_token['token']),
      ('oauth_secret', request_token['secret']),
      ('oauth_callback_confirmed', 'true'),
  ]
  return build_response(body)

@app.route('/oauth/request-tokens/authorize', methods=['GET'])
def show_authorize_request_form():
  '''Show an OAuth token authorization form.'''
  with use_database() as db:
    service = OAuthProviderService(OAuthGateway(db))
    request_token_key = request.args.get('oauth_token', '')
    request_token = service.get_request_token_by_token(request_token_key)

    if not request_token:
      return build_error_response('Request token not found', 404)

    consumer = service.get_consumer_by_id(request_token['consumer_id'])

  return render_template(
      'authorize-request-token.html',
      consumer=consumer,
      request_token=request_token,
  )

@app.route('/oauth/request-tokens/authorize', methods=['POST'])
def authorize_request_token():
  '''Tries to authorize a request token.'''
  with use_database() as db:
    service = OAuthProviderService(OAuthGateway(db))
    request_token_key = request.form.get('oauth_token', '')
    request_token = service.get_request_token_by_token(request_token_key)

    if not request_token:
      return build_error_response('Request token not found', 404)

    if request_token['user_id']:
      return build_error_response('Request token is already authorized', 400)

    username = request.form.get('username', '')
    user = service.get_user_by_username(username)

    if not user:
      return build_error_response('User not found', 404)

    service.authorize_request_token(user['id'], request_token['id'])

    if request_token['callback_url'] == 'oob':
      consumer = service.get_consumer_by_id(request_token['consumer_id'])
      return render_template(
          'display-request-authorization.html',
          consumer=consumer,
          request_token=request_token,
      )

  redirect_url = util.build_oauth_redirect(request_token)
  return redirect(redirect_url)

@app.route('/oauth/access-tokens', methods=['POST'])
def create_access_token():
  '''Creates an access token from a request token.'''
  with use_database() as db:
    service = OAuthProviderService(OAuthGateway(db))
    request_items = normalize_request_params(request)

    try:
      (consumer, _) = service.validate_request(
          util.validate_oauth_access,
          request.method,
          request.base_url,
          request_items,
          'access',
      )
    except Exception as ex:
      return build_error_response(str(ex))

    request_params = dict(request_items)
    request_token_key = request_params['oauth_token']
    request_token = service.get_request_token_by_token(request_token_key)

    try:
      access_token = service.create_access_token(
          consumer,
          request_token,
          request_params['oauth_verifier'],
      )
    except Exception as ex:
      return build_error_response(str(ex))

  body = [
      ('oauth_token', access_token['token']),
      ('oauth_secret', access_token['secret']),
  ]
  return build_response(body)
